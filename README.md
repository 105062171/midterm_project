# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Shopping website   (firebase's project id : Midterm)
* Key functions (add/delete)
    1. Sign in/ Sign up
    2. Host on Firebase
    3. Database read/write
    4. RWD
    5. 加入購物車(刪除)
    6. 結帳
    7. 上傳商品到特定種類(圖片和價錢)
    8. 個人訂單頁面(買單跟賣單的狀況)
    
    
* Other functions (add/delete)
    1. 主頁CSS動畫
    2. Chrome notification
    3. GOOGLE sign in
    4. 釣魚廣告(可改為正當廣告)
    5. 賣家收到訂單之後，可以按出貨，買家訂單會改成已出貨

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description
這邊會將每個html頁面做介紹，每個頁面都有可愛的主頁圖為助教揭開作業序幕。
![image](readme1.PNG)

一開始登入頁面的時候，會有小雷姆歡迎大家，點擊通知會有動人Youtube音樂陪伴各位。
![image](readme2.PNG)

    1. Index.html
    主頁面左上方是論壇LOGO，右上方是帳號登入頁面的連結跟購物車的連結。
    中間是CSS動畫，可以案左右箭頭切換圖片，會以隨機動畫的方式切換。
    下方則分為三個部分:
    下左部分是賣東西的連結跟個人訂單的連結，點"我要賣東C"就可以通往上傳商品頁面。
    下中是所有商品的瀏覽空間，可以按"Add to cart"加入購物車，商品以隨機方式擺放。(按了之後會直接加入，無通知)
    下右則是釣魚廣告，點進去會有特殊連結(只有主頁面跟賣東西頁面連結不同，其他重複)。
    此釣魚廣告也可以更改為正當廣告頁面，正所謂錢乃深愛之物，廣告收入也是重要一環。
    
    
    2. login.html 
    此頁面是很單純的登入頁面，可以創立帳號再登入，也可以直接採用GOOGLE登入。
    幾乎所有功能都只有登入後才看的到。
    登入後會自動回到上一個頁面。
    
    
    3. cart.html
    此頁面會把用戶在主頁面按過的商品加入至此，並會有商品縮圖、金額、賣家信箱、刪除按鈕。
    刪除後只要重整頁面就會提供新的購物車清單。
    按下"買下所有購物車的東西"後，就會把購物清單內的東西都加入到買、賣方的訂單中，並把買方的購物車商品清除。
    
    
    4. sell.html
    此為上傳商品的頁面。
    上傳步驟:
    (1) 登入
    (2) 選擇商品種類(可不選)
    (3) 上傳商品圖片
    (4) 輸入價錢
    (5) 按下"Sell"，即可上傳，即可在主頁面看到商品。
    
    
    (註: 釣魚廣告是精美製作的圖，且連結有別於其他網頁，請注意音量。)

    
    5. order.html 
    此頁面會有登入者的買單跟賣單，買單就是購買的商品訂單，賣單是販賣的商品訂單。
    兩者都會有商品圖、金額、買/賣家、狀態(未送貨/已送貨)。
    買單有刪除按鈕，可以取消訂單，賣家方也會取消該訂單。
    賣單有sell按鈕，按了賣方就會承認訂單，重整頁面後，買方訂單狀況會改成已送貨。


# 作品網址： https://midterm-7fc02.firebaseapp.com/

# Components Description : 

Basic Components:
1. sign up/ sign in: 在"login.html"登入，登入後其他頁面的Account會變成"signout"。
   
2. Host on Firebase: server在firebase
3. Database read/write: 資料都存在Database
4. RWD : 縮放頁面也會更著縮放。


Advanced Components:
1. sign in with GOOGLE:在"login.html"按google按鈕登入。
2. Add Chrome notification: 進入到"index.html"時，會跳出小雷姆通知，點擊有音樂影片。
3. CSS animation: 套用模板，主頁的照片會以特殊方式改變。

# Other Functions Description(1~10%) : 
1. 購物車: 可以挑選商品到頁面中並可刪除，之後再用購物車的商品結帳。
2. 釣魚/廣告頁面: 廣告收入提供吃不飽的可撥工程師一餐溫飽。
3. 個人訂單狀況: 可以看到商品未送貨或已送貨，買家也可主動取消訂單。
4. 上傳商品時，可預覽上傳圖片。

## Security Report (Optional)
購物車、主頁面商品、商品上傳、個人訂單，全部皆需要登入才能使用。

購物車、個人訂單: 只會顯示登入帳號的資訊，不會有看到其他人資訊的疑慮。
主頁面商品: 如果不登入的話，不會顯示任何商品。
商品上傳: 不登入，無法上傳商品。